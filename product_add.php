<?php include_once 'includes/head.php'; ?>
<?php 						
								//get ID, add +1, convert to string. for SKU number.  

		$sku_number = new Product;	
		$sku_num = $sku_number->id() + 1;
		$sku_num = "{$sku_num}";
		$sku = str_pad($sku_num, 8, "0", STR_PAD_LEFT);

						// add all products. Auto add 'FU', 'BO', 'CD' before SKU number by type.
		addProduct();  				
 ?>

<header class="container">
	<div class="header">
		<div class="nav">
		<h1>Product add</h1>
			<div class="nav-option">
			<form action="" id="form" method="post">
				<button name="submit" type="submit"  class="btn-padding">Save</button>
			</form>	

			</div>
		</div>
	</div>
</header>
<main class="container">
	<form>
		<p id="error"></p>
		<div class="form-margin">
			<label for="">SKU</label>
			<input id="sku" form="form" readonly="" type="text" name="sku" value="<?php echo $sku; ?>">
		</div>
		<div class="form-margin">
			<label for="">Name</label>
			<input id="name" form="form" type="text" required="" name="name">
		</div>
		<div   class="form-margin">
			<label for="">Price</label>
			<input id="price" form="form" type="text" required="" name="price">
		</div>
	</form>
	<div class="switcher">
		<label for="">Type switcher</label>
		<select name="" id="switcher" onchange="switchOption();">
			<option value="size">DVD-disc</option>
			<option value="weight">Book</option>
			<option value="dimensions">Furniture</option>
		</select>
	</div>

	<div id="weight" class="switch-container">
		<p id="error-weight"></p>
		<div class="switch-container-content-padding">
			<p>Weight<input form="form" id="weight-req" name="weight" type="text"></p>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit facere neque, beatae ad eum quas iste recusandae explicabo expedita labore inventore nobis quod consectetur eaque, quis et repellendus voluptate, quo!</p>
	</div>

	<div id="dimensions" class="switch-container">
		<p id="error-dimensions"></p>
		<div>
			<p>Height<input form="form" id="height-req" name="height" type="text"></p>
			<p>Width<input form="form" id="width-req" name="width" type="text"></p>
			<p>Length<input form="form" id="length-req" name="length" type="text"></p>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit facere neque, beatae ad eum quas iste recusandae explicabo expedita labore inventore nobis quod consectetur eaque, quis et repellendus voluptate, quo!</p>
	</div>

	<div id="size" class="switch-container">
		<p id="error-size"></p>
		<div>
			<p>Size<input form="form" id="size-req" type="text" name="size"></p>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit facere neque, beatae ad eum quas iste recusandae explicabo expedita labore inventore nobis quod consectetur eaque, quis et repellendus voluptate, quo!</p>
	</div>
	
</main>
<?php include 'includes/footer.php' ?>
