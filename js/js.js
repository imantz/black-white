										//input error form

 document.getElementById("form").addEventListener("click", function(event){

 										//get value from each input

	var switcher = document.getElementById('switcher').value;
	var size = document.getElementById('size-req').value;
	var weight = document.getElementById('weight-req').value;		
	var height = document.getElementById('height-req').value;
	var length = document.getElementById('length-req').value;
	var width = document.getElementById('width-req').value

										//if any option value.length 0 then get error. 

	if(switcher === 'size' && size.length === 0){		
		event.preventDefault();								
		document.getElementById('error-size').innerHTML = "field must be completed";
		document.getElementById('error-size').style.color = "red";

	}else if(switcher === 'weight' && weight.length === 0){
		event.preventDefault();
		document.getElementById('error-weight').innerHTML = "field must be completed";
		document.getElementById('error-weight').style.color = "red";

	}else if(switcher === 'dimensions' && height.length === 0 && width.length === 0 && length.length === 0){
		event.preventDefault();
		document.getElementById('error-dimensions').innerHTML = "fields must be completed";
		document.getElementById('error-dimensions').style.color = "red";
	}
});

										
 				//load default switch option content when page loads.

switchOption();	


function switchOption() {

						//get value from select option, display block and hide elements..

	var valueOption = document.getElementById("switcher").value;

	var switchContent = document.getElementsByClassName("switch-container");
		for (var i = 0; i < switchContent.length; i++) {
			switchContent[i].style.display = "none";
		}								
	document.getElementById(valueOption).style.display = "block";
}


