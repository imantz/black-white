<?php 
class Database {

	private $servername = "localhost";
	private $username = "root";
	private $password = "";
	private $dbname = "traktors20199";
	private $charset = "utf8mb4";

	public function __construct(){

		    $conn = new PDO("mysql:host=".$this->servername, $this->username, $this->password);
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $conn->query("CREATE DATABASE IF NOT EXISTS ".$this->dbname);

		    $this->createTable();
	}

	public function createTable(){
		$this->connect()->query("CREATE TABLE IF NOT EXISTS product (
		id int NOT NULL AUTO_INCREMENT,
	    sku varchar(25),
	    name varchar(25),
	    type varchar(10),
	    price varchar(25),
	    size varchar(25),
	    weight varchar(25),
	    height varchar(25),
	    width varchar(25),
	    length varchar(25),
	    PRIMARY KEY (id))");
	}

	public function connect(){

		try {

			$dsn = "mysql:host=".$this->servername.";dbname=".$this->dbname.";charset=".$this->charset;
			$pdo = new PDO($dsn, $this->username, $this->password);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $pdo;
			
		} catch (PDOException $e) {
			echo "Connection failed: ".$e->getMessage();
		}
	}
}

class Product extends Database {
	public $sku;
	public $name;
	public $price;
	public $type;

	public function id(){
		$stmt = $this->connect()->query("SELECT * FROM product order by id desc limit 1");
 		$stmt->execute();
		foreach($stmt->fetchAll() as $k=> $v){
			return "{$v['id']}";
		}
	}

	public function view_all_cd(){

	    $stmt = $this->connect()->query("SELECT * FROM product WHERE type = 'cd'"); 
	    $stmt->execute();
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
		  ?>
			<div class="col20">
				<input type="checkbox" id="checkbox" form="list-form" name="checkBoxArray[]" value="<?php echo $row['id']; ?>">
				<p><?php echo $row['sku']; ?></p>
				<p><?php echo $row['name']; ?></p>
				<p><?php echo $row['price']; ?>$</p>
				<p>Size: <?php echo $row['size']; ?> Mb</p>
			</div>
		<?php
		}
	}

	public function view_all_book(){
	    $stmt = $this->connect()->query("SELECT * FROM product WHERE type = 'book'"); 
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
		  ?>
			<div class="col20">
				<input type="checkbox" id="checkbox" name="checkBoxArray[]" form="list-form" value="<?php echo $row['id']; ?>">
				<p><?php echo $row['sku']; ?></p>
				<p><?php echo $row['name']; ?></p>
				<p><?php echo $row['price']; ?>$</p>
				<p>Weight: <?php echo $row['weight']; ?> Kg</p>
			</div>
		<?php
		}
	}

	public function view_all_furniture(){
	    $stmt = $this->connect()->query("SELECT * FROM product WHERE type = 'furniture'"); 
	    $stmt->execute();
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
		  ?>
			<div class="col20">
				<input type="checkbox" id="checkbox" name="checkBoxArray[]"  form="list-form" value="<?php echo $row['id']; ?>">
				<p><?php echo $row['sku']; ?></p>
				<p><?php echo $row['name']; ?></p>
				<p><?php echo $row['price']; ?>$</p>
				<p>Dimension: <?php echo $row['height']."x".$row['width']."x".$row['length'] ?></p>
			</div>
		<?php
		}
	}
} // end Product class

class Cd extends Product {

	public $size;

	public function __construct($sku,$name,$price,$size){
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->size = $size;
	}

	public function insert_cd(){
		$sku = $this->sku;
		$name = $this->name;
		$price = $this->price;
		$size = $this->size;

		$stmt = $this->connect()->prepare("INSERT INTO product (sku,name,price,size,type) VALUES ('$sku','$name','$price','$size','cd')");
		$stmt->execute();
	}
}

class Book extends Product {
	public $weight;

	public function __construct($sku,$name,$price,$weight){
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->weight = $weight;
	}

	public function insert_book(){
		$sku = $this->sku;
		$name = $this->name;
		$price = $this->price;
		$weight = $this->weight;
		$stmt = $this->connect()->prepare("INSERT INTO product (sku,name,price,weight,type) VALUES ('$sku','$name','$price','$weight','book')");
		$stmt->execute();		
	}
}

class Furniture extends Product {

	public $height;
	public $width;
	public $length;

	public function __construct($sku,$name,$price, $height,$width,$length){
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;	
		$this->height = $height;
		$this->width = $width;	
		$this->length = $length;
	}


	public function insert_furniture(){

		$sku = $this->sku;
		$name = $this->name;
		$price = $this->price;
		$height = $this->height;
		$width = $this->width;
		$length = $this->length;

		$stmt = $this->connect()->prepare("INSERT INTO product (sku,name,price,height,width,length,type) VALUES ('$sku','$name','$price','$height','$width','$length','furniture')");
		$stmt->execute();
	}
}
?>





