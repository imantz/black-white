<?php 

function deleteAllCheckbox(){
	if(isset($_POST['delete'])){

	$delete= implode(",", $_POST['checkBoxArray']);
	if($delete==''){
		echo "<script>alert('Nothing Selected to Delete!!!'); window.location='product_list.php'</script>";
    }
    $conn = new Database;
    $conn->connect()->query("DELETE FROM product WHERE id IN ($delete) ");

    echo "<script>alert('Successfully Delete!!!'); window.location='product_list.php'</script>";
	}
}

function addProduct(){

	if(isset($_POST['submit'])){
		$sku_number = new Product;			
		$sku_num = $sku_number->id() + 1;
		$sku_num = "{$sku_num}";
		$sku = str_pad($sku_num, 8, "0", STR_PAD_LEFT);

		$name = $_POST['name'];
		
		$price = $_POST['price'];
		$price = number_format($price, 2);

		$weight = $_POST['weight'];
		$height = $_POST['height'];
		$width = $_POST['width'];
		$length = $_POST['length'];
		$size = $_POST['size'];

		if(!empty($name) && !empty($price)){
			if(!empty($size)){
				$sk = "CD".$sku;
				$cd = new Cd($sk,$name,$price,$size);
				$cd->insert_cd();
			}else if(!empty($weight)){
				$sk = "BO".$sku;
				$book = new Book($sk,$name,$price,$weight);
				$book->insert_book();
			}else if(!empty($height) && !empty($width) && !empty($length)){
				$sk = "FU".$sku;
				$book = new Furniture($sk,$name,$price,$height,$width,$length);
				$book->insert_furniture();
			}
		}
	}
}




